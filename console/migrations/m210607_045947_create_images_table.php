<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%images}}`.
 */
class m210607_045947_create_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%images}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(),
            'status' => $this->tinyInteger(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%images}}');
    }
}
