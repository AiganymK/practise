<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m210607_045307_create_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'author' => $this->string(),
            'created_at' => $this->dateTime(),
            'text' => $this->text(),
            'status' => $this->tinyInteger(),
            'image' => $this->string(),
            'title' => $this->string(),
            'updated_at' => $this->dateTime(),
            'title_ru' => $this->string(),
            'text_ru' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
