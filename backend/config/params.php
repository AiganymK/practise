<?php
return [
    'adminEmail' => 'admin@example.com',
    'uploadHostInfo' => 'http://back.local.loc/images', // Показываем отсюда
    'uploadPath' => dirname(__DIR__, 2) . '/backend/web/images/', // Загружаем сюда,
    'uploadImagePath' => dirname(__DIR__, 2) . '/frontend/web/images/', // Загружаем сюда,
];
