<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'tableR'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'Category',
            'description',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => [
                    '0' => Yii::t('app', 'Active'),
                    '1' => Yii::t('app', 'Inactive'),
                ],
                'value' => function ($model) {
                    $label = $model->StatusLabel;
                    return \yii\helpers\Html::tag(
                        'span',
                        $label,
                        [
                            'class' => 'label label-' . ($label == Yii::t('app', 'Inactive') ? 'danger' : 'success'),
                        ]
                    );
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
