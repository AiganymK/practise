<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!doctype html>
<html language="<?= Yii::$app->language ?>" xmlns:font-family="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <title><?php echo Yii::t('app','Admin') ?> | <?= Html::encode($this->title) ?></title>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <? $this->head() ?>
</head>

<body style='background-color: #ecf0f1; font-family: "Rubik" , Arial, Helvetica, sans-serif;'>
<? $this->beginBody() ?>
<div style="background:url(/images/railway.jpg) no-repeat center center;height: 304px;"></div>
<div id='particles-js' style="margin-top: -304px;"></div>
<div class='headerbar' style="margin-top: -347px;">
    <div class='head'>
        <div class='headersleft'>
            <button type="button" style="background-color: inherit; border: none;" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bars"></i><?php echo Yii::t('app', 'Quick links') ?>
            </button>
            <div class="dropdown-menu" style="position: absolute;
    background-color: inherit;top:25px">
                <a class="dropdown-item" href="/"
                   style="color: white;"><?php echo Yii::t('app', 'Home') ?></a><br>
                <a class="dropdown-item" href="/news"
                   style="color: white;"><?php echo Yii::t('app', 'News') ?></a><br>
                <a class="dropdown-item" href="/categories"
                   style="color: white;"><?php echo Yii::t('app', 'Categories') ?></a><br>
                <a class="dropdown-item" href="/topics"
                   style="color: white;"><?php echo Yii::t('app', 'Topics') ?></a><br>
                <a class="dropdown-item" href="http://front.local.loc/ru"
                   style="color: white;"><?php echo Yii::t('app', 'Frontend') ?></a><br>
            </div>
        </div>
        <div class='headersleft'>
            <a href="#" style="color: white;">
                <i class="fas fa-question-circle"></i><?php echo Yii::t('app', 'FAQ') ?>
            </a>
        </div>
    </div>
    <div class='header'>
        <a href="#">
            <div>
                <img src='/images/logo.png' style='width: 220px'>
            </div>
        </a>
        <h1 style='font-size: 24px;'><?php echo Yii::t('app', 'PlanetStyles Test Forum') ?></h1>
        <p class='hidden-sm hidden-xs'
           style='font-size: 12px;'><?php echo Yii::t('app', 'Premium Style Demo by PlanetStyles.net. You can put a long message here by using the "Site Description" field in the ACP :)') ?></p>
    </div>
</div>
<a href="#" class="scroll"><span class="fa fa-arrow-up"></span></a>
<div class='containercontent col-xs-12'>
    <?= Breadcrumbs::widget([
        'homeLink' => ['label' => Yii::t('app', 'Home'), 'url' => '/'],
        'class' => 'upbreadcrumb',
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <div class="col-md-9 col-xs-12">
        <?= $content ?>
        <div>
            <details open>
                <summary class='in'><i class="fas fa-chart-line"></i> <?php echo Yii::t('app', 'Information') ?>
                </summary>
                <div class='information'>
                    <div class='inInformation'>
                        <div>
                            <h3>
                                <a href="#" class='href'><?php echo Yii::t('app', 'Who is online') ?></a>
                            </h3>
                            <?php echo Yii::t('app', 'In total there are <strong>2</strong> users online :: 0 registered, 0 hidden and 2 guests (based on users active over the past 5 minutes)
                                <br>Most users ever online was <strong>9</strong> on Sat Jan 04, 2020 9:46 am<br>
                                <br>Registered users: No registered users
                                <br><strong>Legend: <a href="#">Administrators</a>, <a style="color:#00AA00" href="#">Global moderators</a>, <a href="#">Registered users</a></strong>') ?>
                        </div>
                        <hr style='width: 100%'>
                        <div>
                            <h3 class='href'><?php echo Yii::t('app', 'Statistics') ?></h3>
                            <p style='margin-bottom: 0;'>
                                <?php echo Yii::t('app', 'Total posts <strong>58</strong> • Total topics <strong>38</strong> •Total members <strong>7</strong> • Our newest member <strong><a href="#">test 3</a></strong>') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </details>
        </div>
    </div>
    <div class='col-md-3 col-xs-12'>
        <div>
            <input type='search' class='search inputbox' placeholder="<?php echo Yii::t('app', 'Search...') ?>">
            <button class='button1' type="submit"><i class='icon fas fa-search'></i></button>
            <button class='setting1' type='submit'><i class='fas fa-cog'></i></button>
        </div>
        <div class='separator' style='width: 100%'></div>
        <a href="<?= \yii\helpers\Url::to(['http://front.local.loc/site/logout']) ?>"><?php echo Yii::t('app', 'Logout') ?></a>
        <div>
            <div>
                <img src='/images/right-sidebar.jpg' style='width: 213px;'>
                <div class='separator' style='width: 100%'></div>
                <?php echo Yii::t('app', 'Our Friends') ?>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg'
                                                            style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg'
                                                            style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg'
                                                            style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg'
                                                            style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg'
                                                            style='width: 100%; margin-top:10%;'></a>
            </div>
        </div>
        <details>
            <summary class='in' style='background-color: #02acff;'>
                <?php echo Yii::t('app', 'Blank Widget (Inherits Cat. Style)') ?>
            </summary>
            <div class='information'>
                <div class='inInformation'>
                    <?php echo Yii::t('app', 'Some information goes here! :)') ?>
                </div>
            </div>
        </details>
        <details>
            <summary class='in'>
                <?php echo Yii::t('app', 'Blank Widget (Alt Block)') ?>
            </summary>
            <div class='information'>
                <div class='inInformation'>
                    <?php echo Yii::t('app', 'Some information goes here! :)') ?>
                </div>
            </div>
        </details>
        <div class='separator' style='width: 100%;    float: left;'></div>
        <div style='margin-bottom: 30px;'>
            <p class='widget'><?php echo Yii::t('app', 'Blank Widget (Fancy Panel)') ?></p>
            <hr style="width: 100%;">
            <?php echo Yii::t('app', 'Some things go here :)') ?>
        </div>
    </div>
</div>

<footer>
    <div class='footer'>
        <div class='footer1'>
            <div>
                <i class="fab fa-twitter appsicon transition-scale"></i>
                <i class="fab fa-apple appsicon transition-scale"></i>
                <i class="fab fa-facebook-f appsicon transition-scale"></i>
                <i class="fab fa-codepen appsicon transition-scale"></i>
                <i class="fab fa-google-plus-g appsicon transition-scale"></i>
                <i class="fab fa-digg appsicon transition-scale"></i>
                <i class="fab fa-pinterest-p appsicon transition-scale"></i>
            </div>
        </div>
        <div class='footer2'>
            <?= $this->render('language') ?>
            <?= Breadcrumbs::widget([
                'homeLink' => ['label' => 'Home', 'url' => '/'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <a href="#">
                <i class="fas fa-envelope"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Contact us') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-shield-alt"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'The team') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-check"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Terms') ?></span>
            </a> <a href="#">
                <i class="fas fa-lock"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Privacy') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-users"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Members') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-trash"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Delete cookies') ?></span>
            </a>
            <span style='color: gray' class='hidden-xs'><?php echo Yii::t('app', 'All times are UTC') ?></span>
        </div>
        <div class='footer3'> <?php echo Yii::t('app', 'Powered by') ?> <a href='#'>phpBB™</a>
            • <?php echo Yii::t('app', 'Design by') ?> <a href='#'>PlanetStyles</a></div>
    </div>
</footer>
<? $this->endBody() ?>
</body>

<?php $this->endPage() ?>