<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Images */
/* @var $form yii\widgets\ActiveForm */

$modelForm = new \backend\models\ImagesForm();
?>

<div class="images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelForm, 'name')->textInput(['maxlength' => true]) ?>

    <img src="<?= $model->getImagePath() ?>" alt="" width="300">
    <?= $form->field($modelForm, 'image')->fileInput() ?>

    <?= $form->field($modelForm, 'status')->dropDownList(\backend\models\Images::getStatuses()); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
