<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Images');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Images'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'tableR'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'value' => function ($model) {
                    return Html::img($model->getImagePath(), ['width' => 100, 'alt' => $model->image]);
                },
                'label' => 'Image',
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => [
                    '0' => Yii::t('app', 'Active'),
                    '1' => Yii::t('app', 'Inactive'),
                ],
                'value' => function ($model) {
                    $label = $model->StatusLabel;
                    return \yii\helpers\Html::tag(
                        'span',
                        $label,
                        [
                            'class' => 'label label-' . ($label == Yii::t('app', 'Inactive') ? 'danger' : 'success'),
                        ]
                    );
                },
            ],
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
