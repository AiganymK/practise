<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>

<div class="col-md-9 col-xs-12"><h1><?php echo Yii::t('app', "You're on admin page") ?></h1>
    <a href="<?= \yii\helpers\Url::to(['/site/logout']) ?>"><?php echo Yii::t('app', 'Logout') ?></a>
</div>