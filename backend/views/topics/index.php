<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TopicsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Topics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topics-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Topics'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions'=>[
            'class'=>'tableR'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title:ntext',
            'description:ntext',
            [
                'attribute'=>'images',
                'format'=>'html',
                'value' => function ($model) {
                    return Html::img($model->getImagePath(), ['width' => 100, 'alt' => $model->image]);
                },
                'filter'=>false,
            ],
            'parent_category',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
