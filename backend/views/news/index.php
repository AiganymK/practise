<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create News'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'tableR'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'author',
            'created_at',
            [
                'attribute' => 'title',
//                'visible' => Yii::$app->language=='en',
            ],
            [
                'attribute' => 'text',
//                'visible' => Yii::$app->language=='en',
                'format' => 'html',
                'value' => function ($model) {
                    return StringHelper::truncate($model->text, 100);
                }
            ],
            [
                'attribute' => 'title_ru',
//                'visible' => Yii::$app->language=='ru',
            ],
            [
                'attribute' => 'text_ru',
//                'visible' => Yii::$app->language=='ru',
                'format' => 'html',
                'value' => function ($model) {
                    return StringHelper::truncate($model->text_ru, 100);
                }
            ],
            [
                'value' => function ($model) {
                    return Html::img($model->getImagePath(), ['width' => 100, 'alt' => $model->image]);
                },
                'label' => 'Image',
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => [
                    '0' => Yii::t('app', 'Active'),
                    '1' => Yii::t('app', 'Inactive'),
                ],
                'value' => function ($model) {
                    $label = $model->StatusLabel;
                    return \yii\helpers\Html::tag(
                        'span',
                        $label,
                        [
                            'class' => 'label label-' . ($label == Yii::t('app', 'Inactive') ? 'danger' : 'success'),
                        ]
                    );
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
