<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\News */
if (Yii::$app->language == 'ru') {
    $this->title = Yii::t('app', 'Update News: {name}', [
        'name' => $model->title_ru,
    ]);
} else {
    $this->title = Yii::t('app', 'Update News: {name}', [
        'name' => $model->title,
    ]);
}

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="news-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('language') ?>
    <?= $this->render('_form', [
        'model' => $model,
        'modelForm' => $modelForm,
    ]) ?>

</div>
