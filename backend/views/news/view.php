<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">

    <h1>
        <!--        --><?php //if(Yii::$app->language=='ru'){
        //          Html::encode($this->title_ru)  ;
        //        }else  {
        //             Html::encode($this->title)  ;
        //        } ?>

    </h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => [
            'class' => 'tableT',
        ],
        'attributes' => [
            'author',

            'created_at:date',
            [
                'attribute' => 'title',
//                'visible' => Yii::$app->language=='en',
            ],
            [
                'attribute' => 'text',
//                'visible' => Yii::$app->language=='en',
                'format' => 'html',
                'value' => function ($model) {
                    return StringHelper::truncate($model->text, 100);
                }
            ],
            [
                'attribute' => 'title_ru',
//                'visible' => Yii::$app->language=='ru',
            ],
            [
                'attribute' => 'text_ru',
//                'visible' => Yii::$app->language=='ru',
                'format' => 'html',
                'value' => function ($model) {
                    return StringHelper::truncate($model->text_ru, 100);
                }
            ],
            [
                'value' => function ($model) {
                    return Html::img($model->getImagePath(), ['width' => 200]);
                },
                'label' => Yii::t('app', 'Image'),
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => $label = $model->StatusLabel,
            ],
        ],
    ]) ?>

</div>
