<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */

$modelForm = new \backend\models\NewsForm();
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelForm, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelForm, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($modelForm, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelForm, 'text_ru')->textarea(['rows' => 6]) ?>

    <img src="<?= $model->getImagePath() ?>" alt="" width="300">

    <?= $form->field($modelForm, 'image')->fileInput() ?>

    <?= $form->field($modelForm, 'status')->dropDownList(\backend\models\News::getStatuses()); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
