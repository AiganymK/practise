<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
use backend\models\Images;

class ImagesForm extends Model
{
    /**
     * @var UploadedFile
     * Здесь хранится экземпляр класса UploadedFile
     */
    public $image;
    public $status;
    public $name;
    private $_model;
    private $id;

    public function __construct(Images $model = null, $config = [])
    {
        if ($model) {
            $this->image = $model->image;
            $this->status = $model->status;
            $this->name = $model->name;
            $this->id = $model->id;
            $this->_model = $model;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['status', 'name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'image',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'checkExtensionByMimeType' => true,
                'maxSize' => 5120000, // 500 килобайт = 500 * 1024 байта = 512 000 байт
                'tooBig' => 'Limit is 500KB'
            ],
        ];
    }

    public function uploadImage(UploadedFile $image, $currentImage = null)
    {
        if (!is_null($currentImage))
            $this->deleteCurrentImage($currentImage);
        $this->image = $image;
        if ($this->validate())
            return $this->saveImage();
        return false;
    }


    PUBLIC function getUploadPath()
    {
        return Yii::$app->params['uploadImagePath'] . '';
    }


    /**
     * @return string
     */
    public function generateFileName()
    {
        do {
            $name = substr(md5(microtime() . rand(0, 1000)), 0, 5);
            $file = strtolower($name . '.' . $this->image->extension);
        } while (file_exists($file));
        return $file;
    }

    public function deleteCurrentImage($currentImage)
    {
        if ($currentImage && $this->fileExists($currentImage)) {
            unlink($this->getUploadPath() . $currentImage);
        }
    }

    /**
     * @param $currentFile
     * @return bool
     */
    public function fileExists($currentFile)
    {
        $file = $currentFile ? $this->getUploadPath() . $currentFile : null;
        return file_exists($file);
    }

    /**
     * @return string
     */
    public function saveImage()
    {
        $filename = $this->generateFilename();
        $this->image->saveAs($this->getUploadPath() . $filename);
        return $filename;
    }
}