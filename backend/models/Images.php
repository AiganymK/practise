<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property string $image
 * @property int $status
 * @property string $name
 */
class Images extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 0;
    const STATUS_DEACTIVE = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'name'], 'required'],
            [['status'], 'integer'],
            [['image', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DEACTIVE => Yii::t('app', 'Inactive'),
        ];
    }

    public function getStatusLabel()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }
//    public function getUsers()
//    {
//        return $this->hasOne(User::className(), ['username' => 'author']);
//    }


    public function getImagePath()
    {
        if ($this->image)
            return $this->getImage($this->image);
        return 'https://via.placeholder.com/300x200'; // Default image
    }

    private function getImage($filename)
    {
        return Yii::$app->params['uploadHostInfo'] . '/' . $filename;
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function deleteImage()
    {
        $form = new ImagesForm();
        $form->deleteCurrentImage($this->image);
    }
}
