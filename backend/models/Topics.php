<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "topics".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $parent_category
 * @property string $images
 */
class Topics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'image', 'parent_category', 'images'], 'required'],
            [['title', 'description', 'image'], 'string'],
            [['parent_category', 'images'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'parent_category' => Yii::t('app', 'Parent Category'),
            'images' => Yii::t('app', 'Images'),
        ];
    }

    public function getImagePath()
    {
        if ($this->image)
            return $this->getImage($this->image);
        return 'https://via.placeholder.com/300x200'; // Default image
    }

    private function getImage($filename)
    {
        return Yii::$app->params['uploadHostInfo'] . '/' . $filename;
    }
}
