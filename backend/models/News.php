<?php

namespace backend\models;

use backend\models\NewsForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use frontend\models\User;

/**
 * This is the model class for table "news".
 *
 * @property int|null $author
 * @property string $created_at
 * @property string $text
 * @property int $status
 * @property string $image
 * @property string $title
 * @property int $id
 * @property string $updated_at
 * @property string $title_ru
 * @property string $text_ru
 */
class News extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 0;
    const STATUS_DEACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author', 'status'], 'integer'],
            [['status', 'image'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['text', 'text_ru'], 'string'],
            [['image', 'title', 'title_ru'], 'string', 'max' => 255],
        ];
    }


    public function behaviors()
    {
        return [
            //Использование поведения TimestampBehavior ActiveRecord
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'value' => function () {
                    return gmdate("Y-m-d H:i:s");
                },
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'author' => Yii::t('app', 'Author'),
            'created_at' => Yii::t('app', 'Created At'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'image' => Yii::t('app', 'Image'),
            'title' => Yii::t('app', 'Title'),
            'id' => Yii::t('app', 'ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'text_ru' => Yii::t('app', 'Text Ru'),
        ];
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_DEACTIVE => Yii::t('app', 'Inactive'),
        ];
    }

    public function getStatusLabel()
    {
        return ArrayHelper::getValue(static::getStatuses(), $this->status);
    }
//    public function getUsers()
//    {
//        return $this->hasOne(User::className(), ['username' => 'author']);
//    }


    public function getImagePath()
    {
        if ($this->image)
            return $this->getImage($this->image);
        return 'https://via.placeholder.com/300x200'; // Default image
    }

    private function getImage($filename)
    {
        return Yii::$app->params['uploadHostInfo'] . '/' . $filename;
    }

    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function deleteImage()
    {
        $form = new NewsForm();
        $form->deleteCurrentImage($this->image);
    }
}
