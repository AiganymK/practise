<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;
use backend\models\News;

class NewsForm extends Model
{
    /**
     * @var UploadedFile
     * Здесь хранится экземпляр класса UploadedFile
     */
    public $image;
    public $author;
    public $created_at;
    public $updated_at;
    public $text;
    public $status;
    public $title;
    public $title_ru;
    public $text_ru;
    private $_model;
    private $id;

    public function __construct(News $model = null, $config = [])
    {
        if ($model) {
            $this->author = $model->author;
            $this->text = $model->text;
            $this->text_ru = $model->text_ru;
            $this->status = $model->status;
            $this->created_at = $model->created_at;
            $this->updated_at = $model->updated_at;
            $this->title = $model->title;
            $this->title_ru = $model->title_ru;
            $this->id = $model->id;
            $this->_model = $model;
        }
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['text', 'status', 'title', 'title_ru', 'text_ru'], 'required'],
            [['created_at', 'updated_at', 'image', 'id'], 'safe'],
            [['text', 'text_ru'], 'string'],
            [['status'], 'integer'],
            [['author'], 'integer', 'max' => 30],
            [['title', 'title_ru'], 'string', 'max' => 255],
            [['image'], 'image',
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'checkExtensionByMimeType' => true,
                'maxSize' => 5120000, // 500 килобайт = 500 * 1024 байта = 512 000 байт
                'tooBig' => 'Limit is 500KB'
            ],
        ];
    }

    public function uploadImage(UploadedFile $image, $currentImage = null)
    {
        if (!is_null($currentImage))
            $this->deleteCurrentImage($currentImage);
        $this->image = $image;
        if ($this->validate())
            return $this->saveImage();
        return false;
    }


    PUBLIC function getUploadPath()
    {
        return Yii::$app->params['uploadPath'] . '';
    }


    /**
     * @return string
     */
    public function generateFileName()
    {
        do {
            $name = substr(md5(microtime() . rand(0, 1000)), 0, 5);
            $file = strtolower($name . '.' . $this->image->extension);
        } while (file_exists($file));
        return $file;
    }

    public function deleteCurrentImage($currentImage)
    {
        if ($currentImage && $this->fileExists($currentImage)) {
            unlink($this->getUploadPath() . $currentImage);
        }
    }

    /**
     * @param $currentFile
     * @return bool
     */
    public function fileExists($currentFile)
    {
        $file = $currentFile ? $this->getUploadPath() . $currentFile : null;
        return file_exists($file);
    }

    /**
     * @return string
     */
    public function saveImage()
    {
        $filename = $this->generateFilename();
        $this->image->saveAs($this->getUploadPath() . $filename);
        return $filename;
    }
}