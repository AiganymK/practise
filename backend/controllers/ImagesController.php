<?php

namespace backend\controllers;

use backend\models\ImagesForm;
use Yii;
use backend\models\Images;
use backend\models\ImagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ImagesController implements the CRUD actions for Images model.
 */
class ImagesController extends Controller
{
    public $image;
    public $name;
    public $status;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Images models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Images model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Images model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Images();
        $modelForm = new ImagesForm();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            if ($image = UploadedFile::getInstance($modelForm, 'image')) {
                $model->image = $modelForm->uploadImage($image);
            }
            $model->name = $modelForm->name;
            $model->status = $modelForm->status;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);

            }
            else {
                print_r($model->errors);
                die();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelForm' => $modelForm
        ]);
    }

    /**
     * Updates an existing Images model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelForm = new ImagesForm($model);
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            if ($image = UploadedFile::getInstance($modelForm, 'image')) {
                $model->image = $modelForm->uploadImage($image, $model->image);
            }
            $model->name = $modelForm->name;
            $model->status = $modelForm->status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Все прошло удачно');
                return $this->redirect(['view', 'id' => $model->id]);
//                print_r($model);die;
            }

        }
        return $this->render('update', [
            'model' => $model,
            'modelForm' => $modelForm,
        ]);
    }

    /**
     * Deletes an existing Images model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $model->deleteImage();
            $model->image = '';
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Изображение удалено!');
            }
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }
    /**
     * Finds the Images model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Images the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Images::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
