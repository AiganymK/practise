<?php

namespace backend\controllers;

use Yii;
use backend\models\News;
use backend\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use backend\models\NewsForm;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{

    public $title;
    public $title_ru;
    public $text;
    public $text_ru;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $modelForm = new NewsForm();
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            $model->author = Yii::$app->user->id;
            if ($image = UploadedFile::getInstance($modelForm, 'image')) {
                $model->image = $modelForm->uploadImage($image);
            }
            $model->text_ru = $modelForm->text_ru;
            $model->title_ru = $modelForm->title_ru;
            $model->created_at = $modelForm->created_at;
            $model->updated_at = $modelForm->updated_at;
            $model->title = $modelForm->title;
            $model->text = $modelForm->text;
            $model->status = $modelForm->status;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                print_r($model->errors);
                die();
            }
        }
        return $this->render('create', [
            'model' => $model,
            'modelForm' => $modelForm
        ]);

    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelForm = new NewsForm($model);
        if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
            if ($image = UploadedFile::getInstance($modelForm, 'image')) {
                $model->image = $modelForm->uploadImage($image, $model->image);
            }
            $model->text_ru = $modelForm->text_ru;
            $model->title_ru = $modelForm->title_ru;
            $model->title = $modelForm->title;
            $model->text = $modelForm->text;
            $model->status = $modelForm->status;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Все прошло удачно');
                return $this->redirect(['view', 'id' => $model->id]);
//                print_r($model);die;
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelForm' => $modelForm,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public
    function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $model->deleteImage();
            $model->image = '';
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Изображение удалено!');
            }
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
