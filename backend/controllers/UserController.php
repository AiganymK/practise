<?php
/**
 * Created by PhpStorm.
 * User: фора
 * Date: 07.06.2021
 * Time: 11:15
 */

namespace backend\controllers;


use common\models\LoginForm;
use yii\web\Controller;

class UserController extends Controller
{
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

}