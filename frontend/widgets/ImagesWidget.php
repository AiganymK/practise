<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 18 авг 2020 г.
 * Time: 11:12
 */

namespace frontend\widgets;

use yii\base\Widget;
use backend\models\Images;


class ImagesWidget extends Widget
{
  public function init()
    {
        parent::init();
    }

    public function run()
    {
        $images = Images::find()->all();
        return $images;
    }
}