<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int|null $author
 * @property string $created_at
 * @property string $text
 * @property int $status
 * @property string $image
 * @property string $title
 * @property int $id
 * @property string $updated_at
 * @property string $title_ru
 * @property string $text_ru
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author', 'status'], 'integer'],
            [['created_at', 'status', 'image', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['text', 'text_ru'], 'string'],
            [['image', 'title', 'title_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'author' => Yii::t('app', 'Author'),
            'created_at' => Yii::t('app', 'Created At'),
            'text' => Yii::t('app', 'Text'),
            'status' => Yii::t('app', 'Status'),
            'image' => Yii::t('app', 'Image'),
            'title' => Yii::t('app', 'Title'),
            'id' => Yii::t('app', 'ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'text_ru' => Yii::t('app', 'Text Ru'),
        ];
    }
}
