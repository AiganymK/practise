<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 24 июл 2020 г.
 * Time: 10:49
 */

namespace frontend\models;

use yii\base\Model;
use yii\db\ActiveRecord;


class RegistrationForm extends Model
{
    /**
     * {@inheritdoc}
     */

    public $username;
    public $email;
    public $password;
    public $password_repeat;

    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'email'], 'required'],
            ['username', 'string', 'length' => [5, 10]],
            ['email', 'email'],
            ['password', 'string', 'length' => [5, 10]],
            ['password', 'match', 'pattern' => '#\d#s', 'message' => 'Пароль должен содержать цифры'],
            ['password', 'match', 'pattern' => '#[a-z]#is', 'message' => 'Пароль должен содержать латинские буквы'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не идентичны'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

}