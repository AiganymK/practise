<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\ImagesWidget;
use frontend\controllers\ImagesController;
AppAsset::register($this);
$images=\backend\models\Images::find()->all();
?>

<?php $this->beginPage() ?>
<!doctype html>
<html xmlns:font-family="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title><?= Html::encode($this->title) ?></title>
    <? $this->head() ?>
</head>

<body style='background-color: #ecf0f1; font-family: "Rubik" , Arial, Helvetica, sans-serif;'>
<? $this->beginBody() ?>
<div style="height: 304px;"></div>
<!--<div id='particles-js'></div>-->
<div class='headerbar'>
    <div class='head'>
        <div class='headersleft'>
            <button type="button" style="background-color: inherit; border: none;" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bars"></i><?php echo Yii::t('app', 'Quick links') ?>
            </button>
            <div class="dropdown-menu" style="background-color: inherit;
    margin-top: -287px;">
                <a class="dropdown-item" href="/"><?php echo Yii::t('app', 'Home') ?></a><br>
                <a class="dropdown-item" href="h/news"><?php echo Yii::t('app', 'News') ?></a><br>
                <a class="dropdown-item" href="http://back.local.loc"><?php echo Yii::t('app', 'Backend') ?></a><br>
            </div>
        </div>
        <div class='headersleft'>
            <a href="#">
                <i class="fas fa-question-circle"></i><?php echo Yii::t('app', 'FAQ') ?>
            </a>
        </div>
        <div class='headersright'>
            <a href="?r=site/login">
                <i class="fas fa-power-off"></i><?php echo Yii::t('app', 'Login') ?>
            </a>
        </div>
        <div class='headersright'>
            <a href="?r=site/registration">
                <i class="far fa-edit"></i><?php echo Yii::t('app', 'Register') ?>
            </a>
        </div>
        <div class='headersright'>
            <a href="?r=site/admin">
                <i class="fas fa-dragon"></i><?php echo Yii::t('app', 'Admin') ?>
            </a>
        </div>
    </div>
    <div class='header'>
        <a href="#">
            <div>
                <img src='/images/logo.png' style='width: 220px'>
            </div>
        </a>
        <h1 style='font-size: 24px;'><?php echo Yii::t('app', 'PlanetStyles Test Forum') ?></h1>
        <p class='hidden-sm hidden-xs'
           style='font-size: 12px;'><?php echo Yii::t('app', 'Premium Style Demo by PlanetStyles.net. You can put a long message here by using the "Site Description" field in the ACP :)') ?></p>
    </div>
</div>

<div class="sim-slider">
    <ul class="sim-slider-list">
<!--        <li class="sim-slider-element"><img src="/images/railway.jpg" alt="0"></li>-->
<!--        <li class="sim-slider-element"><img src="/images/lake.jpg" alt="1"></li>-->
<!--        <li class="sim-slider-element"><img src="/images/new-york.jpg" alt="2"></li>-->
<!--        <li class="sim-slider-element"><img src="/images/comp.jpg" alt="3"></li>-->
<!--        <li class="sim-slider-element"><img src="/images/dom.jpg" alt="4"></li>-->
        <?php //\frontend\widgets\ImagesWidget ::widget() ?>
        <?php foreach ($images as $image): ?>
        <li class="sim-slider-element"><img src="/images/<?= $image['image'] ?>" alt="4"></li>
        <?php endforeach; ?>

    </ul>
    <div class="sim-slider-arrow-left" style="background-color: white;
    border-radius: 50%;
    height: 40px;
    width: 40px;
    background-size: 14px;
    background-position: 40%;"></div>
    <div class="sim-slider-arrow-right" style="background-color: white;
    border-radius: 50%;
    height: 40px;
    width: 40px;
    background-size: 14px;
    background-position: 50%;"></div>
    <div class="sim-slider-dots"></div>
</div>
<a href="#" class="scroll"><span class="fa fa-arrow-up"></span></a>
<div class='containercontent col-xs-12'>

    <?= Breadcrumbs::widget([
        'homeLink' => ['label' => Yii::t('app', 'Home'), 'url' => '/'],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <div class='col-md-9 col-xs-12'>
        <?= $content ?>
    </div>

    <div class='col-md-3 col-xs-12'>
        <div>
            <form action="<?= \yii\helpers\Url::to(['news/search']) ?>" method="get">
                <input name='q' type='search' class='search inputbox' placeholder="<?php echo Yii::t('app', 'Search...') ?>">
                <button class='button1' type="submit"><i class='icon fas fa-search'></i></button>
                <button class='setting1' type='submit'><i class='fas fa-cog'></i></button>
            </form>
        </div>
        <div class='separator' style='width: 100%'></div>
        <div>
            <?php if (!Yii::$app->user->isGuest): ?>
                <a href="<?= \yii\helpers\Url::to(['http://front.local.loc/?r=site/logout']) ?>"><?php echo Yii::t('app', 'Logout') ?></a>
            <?php endif ?>
            <div>
                <img src='/images/right-sidebar.jpg' style='width: 213px;'>
                <div class='separator' style='width: 100%'></div>
                <?php echo Yii::t('app', 'Our Friends') ?>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg' style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg' style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg' style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg' style='width: 100%; margin-top:10%;'></a>
                <a class='col-xs-6 col-md-12' href='#'><img src='/images/goodbye.jpg' style='width: 100%; margin-top:10%;'></a>
            </div>
        </div>
        <details>
            <summary class='in'
                     style='background-color: #02acff;'><?php echo Yii::t('app', 'Blank Widget (Inherits Cat. Style)') ?></summary>
            <div class='information'>
                <div class='inInformation'><?php echo Yii::t('app', 'Some information goes here! :)') ?></div>
            </div>
        </details>
        <details>
            <summary class='in'><?php echo Yii::t('app', 'Blank Widget (Alt Block)') ?></summary>
            <div class='information'>
                <div class='inInformation'><?php echo Yii::t('app', 'Some information goes here! :)') ?></div>
            </div>
        </details>
        <div class='separator' style='width: 100%;    float: left;'></div>
        <div style='margin-bottom: 30px;'>
            <p class='widget'><?php echo Yii::t('app', 'Blank Widget (Fancy Panel)') ?></p>
            <hr style="width: 100%;">
            <?php echo Yii::t('app', 'Some things go here :)') ?>
        </div>
    </div>


</div>

<footer>
    <div class='footer'>
        <div class='footer1'>
            <div>
                <i class="fab fa-twitter appsicon transition-scale"></i>
                <i class="fab fa-apple appsicon transition-scale"></i>
                <i class="fab fa-facebook-f appsicon transition-scale"></i>
                <i class="fab fa-codepen appsicon transition-scale"></i>
                <i class="fab fa-google-plus-g appsicon transition-scale"></i>
                <i class="fab fa-digg appsicon transition-scale"></i>
                <i class="fab fa-pinterest-p appsicon transition-scale"></i>
            </div>
        </div>
        <div class='footer2'>
            <?= $this->render('language') ?>
            <?= Breadcrumbs::widget([
                'homeLink' => ['label' => 'Home', 'url' => '/'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <a href="#">
                <i class="fas fa-envelope"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Contact us') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-shield-alt"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'The team') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-check"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Terms') ?></span>
            </a> <a href="#">
                <i class="fas fa-lock"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Privacy') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-users"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Members') ?></span>
            </a>
            <a href="#">
                <i class="fas fa-trash"></i><span
                        class="hidden-sm hidden-xs"><?php echo Yii::t('app', 'Delete cookies') ?></span>
            </a>
            <span style='color: gray' class='hidden-xs'><?php echo Yii::t('app', 'All times are UTC') ?></span>
        </div>
        <div class='footer3'> <?php echo Yii::t('app', 'Powered by') ?> <a href='#'>phpBB™</a>
            • <?php echo Yii::t('app', 'Design by') ?> <a href='#'>PlanetStyles</a></div>
    </div>
</footer>
<? $this->endBody() ?>
</body>

<?php $this->endPage() ?>
