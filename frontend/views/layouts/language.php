<?php

use yii\bootstrap\Html;

if (Yii::$app->language == 'en') {
    echo Html::a('Перейти к русскому', array_merge(Yii::$app->request->get(),
        [Yii::$app->controller->route, 'language' => 'ru']));
} else {
    echo Html::a('Go to English', array_merge(Yii::$app->request->get(),
        [Yii::$app->controller->route, 'language' => 'en']));
}