<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Home');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <?php foreach ($categories AS $item):
        if ($item->status == 0):?>
            <a href="#" class='category col-xs-12  transition3'><?php echo $item['Category'] ?></a>
            <div class='contentcategory col-xs-12'>
                <?php foreach ($topics as $topic):
                    if ($topic->parent_category == $item->id):?>
                        <a href="#" style=' color: black;'>
                            <div class='col-xs-6 col-lg-4' style='padding: 0;' id='fadeIn'>
                                <div class='topic transition-scale'
                                     style="background-image: url('/images/<?php echo $topic['image'] ?>');">
                                    <div class='topicname'>
                                        <div class='topic1'><?php echo $topic['title'] ?></div>
                                        <div class='topicdescription'><?php echo $topic['description'] ?></div>
                                    </div>
                                    <div class='separator'></div>
                                    <div class='bottom'>
                                        <div class='bottomtopic'>
                                            <span class='number'>7</span><br>
                                            <span class='type'>Topics</span>
                                        </div>
                                        <div class='bottomtopic'>
                                            <span class='number'>22</span><br>
                                            <span class='type'>Posts</span>
                                        </div>
                                        <div class='bottomtopic2'>
                                            <span class='data '>Sun Feb 03, 2019 3:55 pm</span> <bR>
                                            <span class='type'>Last post</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?php endif; endforeach; ?>
            </div>
        <?php endif; endforeach; ?>
    <div>
        <details open>
            <summary class='in'><i class="fas fa-chart-line"></i> Information</summary>
            <div class='information'>
                <div class='inInformation'>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <h3 style='margin-top: 5px;'><a href="#" class='href'>Login</a> • <a href="#" class='href'>Register</a>
                        </h3>
                        <?php $form = ActiveForm::begin([
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"\">{input}</div>\n<div class=\"\">{error}</div>",
                                'labelOptions' => ['class' => 'float'],
                                'inputOptions' => [
                                    'class' => 'input float',
                                ],
                            ],
                        ]); ?>
                        <?= $form->field($loginModel, 'username')->textInput()->label('Username:') ?>
                        <?= $form->field($loginModel, 'password')->passwordInput()->label('Password:') ?>
                        <a href=".#" class="float">I forgot my password </a><span class="float">|</span>
                        <?= $form->field($loginModel, 'rememberMe')->checkbox([
                        'template' => "<div class=\"float\">{input} {label}</div>\n<div class=\"\">{error}</div>",
                    ]) ?>
                        <?= Html::submitButton('Login', ['class' => 'login1 transition2 float', 'name' => 'login-button']) ?>
                        <?php ActiveForm::end(); ?>
                        <hr style='width: 100%'>
                    <?php endif; ?>
                    <div>
                        <h3>
                            <a href="#" class='href'>Who is online</a>
                        </h3>
                        <p>In total there are <strong>2</strong> users online :: 0 registered, 0 hidden and 2 guests
                            (based on users active over the past 5 minutes)
                            <br>Most users ever online was <strong>9</strong> on Sat Jan 04, 2020 9:46 am<br>
                            <br>Registered users: No registered users
                            <br><strong>Legend: <a href="#">Administrators</a>, <a style="color:#00AA00" href="#">Global
                                    moderators</a>, <a href="#">Registered users</a></strong></p>
                    </div>
                    <hr style='width: 100%'>
                    <div>
                        <h3 class='href'>Statistics</h3>
                        <p style='margin-bottom: 0;'>
                            Total posts <strong>58</strong> • Total topics <strong>38</strong> •Total members
                            <strong>7</strong> • Our newest member <strong><a href="#">test 3</a></strong>
                        </p>
                    </div>
                </div>
            </div>
        </details>
    </div>
</div>