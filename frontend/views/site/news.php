<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\StringHelper;

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content">
    <h1><?php echo Yii::t('app', 'News') ?></h1>
    <div class='contentcategory col-xs-12'>
        <?php foreach ($news AS $new): ?>
<!--            --><?//=$new['id']?>
            <a href="/newsview?id=<?=$new['id']?>" style=' color: black;'>
                <div class='col-xs-6 col-lg-4' style='padding: 0;width:100%;' id='fadeIn'>
                    <div class='topic transition-scale1'>
                        <img style="height: 20%;width: 20%;" src="http://back/images/<?php echo $new['image']; ?>">
                        <div style="width: 80%; float:left;">
                            <?php if (Yii::$app->language == 'en'): ?>
                                <div class='topic1'><h3><?php echo $new['title'] ?></h3></div>
                                <div class='topicdescription'
                                     style="font-size:15px;"><?php echo StringHelper::truncate($new['text'], 150, '...'); ?></div>
                            <?php endif; ?>
                            <?php if (Yii::$app->language == 'ru'): ?>
                                <div class='topic1'><h3><?php echo $new['title_ru'] ?></h3></div>
                                <div class='topicdescription'
                                     style="font-size:15px;"><?php echo StringHelper::truncate($new['text_ru'], 150, '...'); ?></div>
                            <?php endif; ?>
                        </div>
                        <div class='separator'></div>
                        <div class='bottom'>
                            <div class='bottomtopic'>
                                <span class='number'><?php echo $new['author'] ?></span><br>
                                <span class='type'><?php echo Yii::t('app', 'author') ?></span>
                            </div>
                            <div class='bottomtopic'>
                                <span class='number'><?php echo $new['created_at'] ?></span><br>
                                <span class='type'><?php echo Yii::t('app', 'Created at') ?></span>
                            </div>
                            <div class='bottomtopic2'>
                                <span class='data '><?php echo $new['updated_at'] ?></span> <bR>
                                <span class='type'><?php echo Yii::t('app', 'Updated at') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        <?php endforeach; ?>
    </div>
    <div>
        <details open>
            <summary class='in'><i class="fas fa-chart-line"></i> <?php echo Yii::t('app', 'Information') ?></summary>
            <div class='information'>
                <div class='inInformation'>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <h3 style='margin-top: 5px;'><a href="#" class='href'><?php echo Yii::t('app', 'Login') ?></a> •
                            <a href="#" class='href'><?php echo Yii::t('app', 'Register') ?></a></h3>
                        <?php $form = ActiveForm::begin([
                            'fieldConfig' => [
                                'template' => "{label}\n<div class=\"\">{input}</div>\n<div class=\"\">{error}</div>",
                                'labelOptions' => ['class' => 'float'],
                                'inputOptions' => [
                                    'class' => 'input float',
                                ],
                            ],
                        ]); ?>
                        <?= $form->field($loginModel, 'username')->textInput()->label('Username:') ?>
                        <?= $form->field($loginModel, 'password')->passwordInput()->label('Password:') ?>
                        <a href=".#" class="float"><?php echo Yii::t('app', 'I forgot my password') ?> </a><span
                                class="float">|</span>
                        <?= $form->field($loginModel, 'rememberMe')->checkbox([
                        'template' => "<div class=\"float\">{input} {label}</div>\n<div class=\"\">{error}</div>",
                    ]) ?>
                        <?= Html::submitButton('Login', ['class' => 'login1 transition2 float', 'name' => 'login-button']) ?>
                        <?php ActiveForm::end(); ?>
                        <hr style='width: 100%'>
                    <?php endif; ?>
                    <div>
                        <h3>
                            <a href="#" class='href'><?php echo Yii::t('app', 'Who is online') ?></a>
                        </h3>
                        <p>In total there are <strong>2</strong> users online :: 0 registered, 0 hidden and 2 guests
                            (based on users active over the past 5 minutes)
                            <br>Most users ever online was <strong>9</strong> on Sat Jan 04, 2020 9:46 am<br>
                            <br>Registered users: No registered users
                            <br><strong>Legend: <a href="#">Administrators</a>, <a style="color:#00AA00" href="#">Global
                                    moderators</a>, <a href="#">Registered users</a></strong></p>
                    </div>
                    <hr style='width: 100%'>
                    <div>
                        <h3 class='href'><?php echo Yii::t('app', 'Statistics') ?></h3>
                        <p style='margin-bottom: 0;'>
                            <?php echo Yii::t('app', 'Total posts <strong>58</strong> • Total topics <strong>38</strong> •Total members <strong>7</strong> • Our newest member <strong><a href="#">test 3</a></strong>') ?>
                        </p>
                    </div>
                </div>
            </div>
        </details>
    </div>

</div>