<?php

namespace frontend\controllers;

use backend\models\Images;
use common\models\LoginForm;
use Yii;
use frontend\models\News;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;

/**
 * NewsController implements the CRUD actions for News model.
 */
class ImagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new NewsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionIndex()
    {
        $this->layout = 'main.php';

        $loginModel = new LoginForm();
        $loginModel->password = '';

        $news = Images::find()
            ->select([
                'images.id',
                'images.image',
                'images.name',
            ])
            ->where(['images.status' => \backend\models\Images::STATUS_ACTIVE])
            ->asArray()
            ->all();

//        print_r($news); die;

        return $this->render('index', compact('loginModel', 'images')
        );
    }
    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $loginModel = new LoginForm();
        $loginModel->password = '';

        $news = News::find()
            ->select([
                'images.id',
                'images.image',
                'images.name',
            ])
            ->where(['images.status' => \backend\models\News::STATUS_ACTIVE, 'images.id'=>$id])
            ->asArray()
            ->all();

        return $this->render('view', [
            'model' => $this->findModel($id),
            'loginModel'=>$loginModel, 'images'=>$image
        ]);
    }

//    public function actionSearch($q){
//
////        $loginModel = new LoginForm();
////        $loginModel->password = '';
//
//        $news = News::find()
//            ->select([
//                'news.title',
//                'news.id',
//                'news.title_ru',
//                'news.text',
//                'news.text_ru',
//                'news.image',
//                'news.created_at',
//                'user.username AS author'
//            ])
//            ->leftJoin('user', 'user.id=news.author')
//            ->where(['news.status' => \backend\models\News::STATUS_ACTIVE])
//            ->where (['like','text',$q])
//            ->where (['like','text_ru',$q])
//            ->where (['like','title',$q])
//            ->where (['like','title_ru',$q])
//            ->asArray()
//            ->all();
//        return $this->render('search', compact('news', 'q'));
//    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Images();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
