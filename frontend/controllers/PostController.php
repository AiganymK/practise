<?php

namespace app\frontend\controllers;

use yii\db\ActiveRecord;

class PostController extends ActiveRecord
{
    public static function tableName()
    {
        return 'registration';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'e-mail' => 'E-mail',
            'password' => 'Пароль',
        ];
    }

    public function rules()
    {
        return [
            [['name', 'password', 'email'], 'required'],
            ['e-mail', 'email'],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
